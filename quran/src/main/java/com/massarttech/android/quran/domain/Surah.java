package com.massarttech.android.quran.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Surah implements Serializable {
    private int    number;
    private String name;
    private String englishName;
    private String englishNameTranslation;
    private int    numberOfAyahs;
    private String revelationType;

}