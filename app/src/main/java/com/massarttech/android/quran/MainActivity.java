package com.massarttech.android.quran;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.massarttech.android.quran.domain.Ayah;

import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.massarttech.android.quran.QuranApi.MAX_AYAH;

public class MainActivity extends AppCompatActivity {
    private TextView   tvArabic;
    private TextView   tvEnglish;
    private TextView   tvUrdu;
    private TextView   tvDescription;
    private Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvArabic      = findViewById(R.id.arabic);
        tvEnglish     = findViewById(R.id.english);
        tvUrdu        = findViewById(R.id.urdu);
        tvDescription = findViewById(R.id.description);

    }

    @Override
    protected void onStop() {
        super.onStop();
        disposable.dispose();
    }

    @Override
    protected void onStart() {
        super.onStart();
        int              random  = new Random().nextInt(MAX_AYAH) + 1; //never zero
        Observable<Ayah> arabic  = QuranApi.getInstance().getAyah(random, Language.ARABIC).toObservable();
        Observable<Ayah> urdu    = QuranApi.getInstance().getAyah(random, Language.URDU).toObservable();
        Observable<Ayah> english = QuranApi.getInstance().getAyah(random, Language.ENGLISH).toObservable();

        disposable = Observable.combineLatest(arabic, urdu, english, this::combine)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setupAyah, this::showError);
    }

    private Translated combine(@NonNull Ayah arabic, @NonNull Ayah urdu, @NonNull Ayah english) {
        return new Translated(english, arabic, urdu);
    }

    private void setupAyah(@NonNull Translated ayah) {
        tvArabic.setText(ayah.arabic.getText());
        tvUrdu.setText(ayah.urdu.getText());
        tvEnglish.setText(ayah.english.getText());

        tvDescription.setText(getString(R.string.format_description, ayah.arabic.getSurah().getName(),
                ayah.arabic.getNumberInSurah()));
    }

    private void showError(@NonNull Throwable throwable) {
        tvArabic.setText(throwable.getMessage());
    }

    class Translated {
        private final Ayah english;
        private final Ayah arabic;
        private final Ayah urdu;

        Translated(Ayah english, Ayah arabic, Ayah urdu) {
            this.english = english;
            this.arabic  = arabic;
            this.urdu    = urdu;
        }
    }
}
