package com.massarttech.android.quran.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Ayah implements Serializable {
    private int     number;
    private String  text;
    private Edition edition;
    private Surah   surah;
    private int     numberInSurah;
    private int     juz;
    private int     manzil;
    private int     page;
    private int     ruku;
    private int     hizbQuarter;
    private boolean sajda;

}
