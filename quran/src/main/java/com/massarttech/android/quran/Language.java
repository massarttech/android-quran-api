package com.massarttech.android.quran;

import io.reactivex.annotations.NonNull;

public enum Language {
    ARABIC("ar"), ENGLISH("en.asad"),
    TURKISH("tr.diyanet"), URDU("ur.jalandhry");
    private final String value;

    private Language(String value) {
        this.value = value;
    }

    @Override
    @NonNull
    public String toString() {
        return value;
    }

    @NonNull
    public String getValue() {
        return value;
    }
}
