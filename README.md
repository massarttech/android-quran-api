# Quran API [![](https://jitpack.io/v/com.gitlab.massarttech/android-quran-api.svg)](https://jitpack.io/#com.gitlab.massarttech/android-quran-api)
**Load Ayah from http://api.alquran.cloud/v1/..**

<img src="https://gitlab.com/massarttech/android-quran-api/raw/master/screenshot.png?inline=false" width="300" height="500"/>

### Setup
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:
```
dependencies {
    implementation 'com.gitlab.massarttech:android-quran-api:1.0.0'
}
```

### Usage
```java
  disposable = QuranApi.getInstance().getAyah()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(this::setupAyah, this::showError);
```

****Please do not use in app with Ads****