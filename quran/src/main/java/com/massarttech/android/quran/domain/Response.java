package com.massarttech.android.quran.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Response implements Serializable {
    private int    code;
    private String status;
    @SerializedName("data")
    private Ayah   ayah;

}