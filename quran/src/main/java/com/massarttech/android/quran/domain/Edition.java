package com.massarttech.android.quran.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Edition implements Serializable {
    private String identifier;
    private String language;
    private String name;
    private String englishName;
    private String format;
    private String type;

}