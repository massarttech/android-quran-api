package com.massarttech.android.quran;

import com.massarttech.android.quran.domain.Response;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface EndPoints {
    @GET("ayah/{number}/{lang}")
    Single<Response> getAyah(@Path("number") int number,
                             @Path("lang") String lang);
}
