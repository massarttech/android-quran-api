package com.massarttech.android.quran;

import android.annotation.SuppressLint;

import com.massarttech.android.quran.domain.Ayah;

import java.util.Random;

import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class QuranApi {
    public static final  int      MAX_AYAH = 6236;
    private static final QuranApi instance;

    static {
        instance = new QuranApi();
    }

    private final EndPoints endPoints;

    private QuranApi() {
        endPoints = new Retrofit.Builder()
                .baseUrl("http://api.alquran.cloud/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(EndPoints.class);
    }

    public static QuranApi getInstance() {
        return instance;
    }

    public Single<Ayah> getAyah(@NonNull Language language) {
        int random = new Random().nextInt(MAX_AYAH) + 1; //never zero
        return getAyah(random, language);
    }

    public Single<Ayah> getAyah() {
        return getAyah(Language.ARABIC);
    }

    @SuppressLint("DefaultLocale")
    public Single<Ayah> getAyah(int number, @NonNull Language language) {
        return endPoints.getAyah(number, language.getValue())
                .flatMap(response -> {
                    if (response.getCode() == 200 && response.getAyah() != null) {
                        return Single.just(response.getAyah());
                    } else {
                        return Single.error(new Throwable(String.format("code[%d] message[%s]", response.getCode(), response.getStatus())));
                    }
                });
    }
}
